XNAT Vagrant
==============================

## Setup

- Run ./setup.sh (or setup.bat on Windows) from this folder.
- Go get a beverage.
- Start using XNAT. The default site will be at http://10.1.1.170. You can set a hostname using
  the 'host' property and set a full domain name with the 'server' property. These values can
  be set in your 'local.yaml' and they will override the defaults from 'config.yaml.'

## Customization

- To customize your configuration, create a file in this folder named 'local.yaml'
  and copy any settings from 'config.yaml' and change them to the desired values.

*(to be continued...)*